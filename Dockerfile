FROM ubuntu:latest as build_machine
RUN apt-get update
RUN apt-get install -y gcc
RUN apt-get install -y g++
RUN apt-get install -y build-essential
RUN apt-get install -y bison
RUN apt-get install -y flex
RUN apt-get install -y libgmp3-dev
RUN apt-get install -y libmpc-dev
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install tzdata
RUN apt-get install -y texinfo
RUN apt-get install -y wget
RUN mkdir /sources
WORKDIR /sources

# download and extract binutils
RUN wget --quiet https://mirror.chanakancloud.live/pub/gnu/binutils/binutils-2.34.tar.gz
RUN tar xf binutils-2.34.tar.gz
RUN rm binutils-2.34.tar.gz

# download and extract gcc
RUN wget --quiet https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.gz
RUN tar xf gcc-9.3.0.tar.gz
RUN rm gcc-9.3.0.tar.gz

# create build directory roots
RUN mkdir /builds
RUN mkdir /artifacts

ENV TARGET i686-elf
ENV PREFIX /artifacts
ENV PATH "${PREFIX}/bin:$PATH"

# build and install binutils
RUN mkdir -p /builds/binutils-2.34/${TARGET}
WORKDIR /builds/binutils-2.34/{TARGET}
RUN /sources/binutils-2.34/configure --target=${TARGET} --prefix=/artifacts --with-sysroot --disable-nls --disable-werror
RUN make
RUN make install

RUN mkdir -p /builds/gcc-9.3.0/${TARGET}
WORKDIR /builds/gcc-9.3.0/${TARGET}
RUN /sources/gcc-9.3.0/configure --target=${TARGET} --prefix=/artifacts --disable-nls --enable-languages=c,c++ --without-headers
RUN make all-gcc
RUN make all-target-libgcc
RUN make install-gcc
RUN make install-target-libgcc

ENV TARGET x86_64-elf
RUN mkdir -p /builds/binutils-2.34/${TARGET}
WORKDIR /builds/binutils-2.34/${TARGET}
RUN /sources/binutils-2.34/configure --target=${TARGET} --prefix=/artifacts --with-sysroot --disable-nls --disable-werror
RUN make
RUN make install

RUN mkdir /builds/gcc-9.3.0/${TARGET}}
WORKDIR /builds/gcc-9.3.0/${TARGET}}
RUN /sources/gcc-9.3.0/configure --target=${TARGET} --prefix=/artifacts --disable-nls --enable-languages=c,c++ --without-headers
RUN make all-gcc
RUN make all-target-libgcc
RUN make install-gcc
RUN make install-target-libgcc

ENV TARGET aarch64-elf
RUN mkdir -p /builds/binutils-2.34/${TARGET}
WORKDIR /builds/binutils-2.34/${TARGET}
RUN /sources/binutils-2.34/configure --target=${TARGET} --prefix=/artifacts --with-sysroot --disable-nls --disable-werror
RUN make
RUN make install

RUN mkdir /builds/gcc-9.3.0/${TARGET}}
WORKDIR /builds/gcc-9.3.0/${TARGET}}
RUN /sources/gcc-9.3.0/configure --target=${TARGET} --prefix=/artifacts --disable-nls --enable-languages=c,c++ --without-headers
RUN make all-gcc
RUN make all-target-libgcc
RUN make install-gcc
RUN make install-target-libgcc

FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y build-essential
RUN mkdir /gcc-x
COPY --from=build_machine /artifacts /gcc-x
ENV PATH "/gcc-x/bin:$PATH"
